

  const filmsListElement = document.getElementById("filmsList");

  function fetchFilms() {
    fetch("https://ajax.test-danit.com/api/swapi/films")
      .then((response) => response.json())
      .then((data) => {
        data.forEach((film) => {
          const { episodeId, name, openingCrawl, characters } = film;

          const filmElement = document.createElement("div");
          filmElement.innerHTML = `
            <h3>Episode ${episodeId}: ${name}</h3>
            <p>${openingCrawl}</p>
            <ul id="charactersList${episodeId}"></ul>
          `;

          filmsListElement.appendChild(filmElement);

          fetchCharacters(characters, episodeId);
        });
      })
      .catch((error) => console.error("Error:", error));
  }

  function fetchCharacters(characters, episodeId) {
    const charactersListElement = document.getElementById(`charactersList${episodeId}`);

    characters.forEach((characterUrl) => {
      fetch(characterUrl)
        .then((response) => response.json())
        .then((character) => {
          const { name, birthYear, gender } = character;
          const characterElement = document.createElement("li");
          characterElement.textContent = `Name: ${name}, Birth Year: ${birthYear}, Gender: ${gender}`;
          charactersListElement.appendChild(characterElement);
        })
        .catch((error) => console.error("Error:", error));
    });
  }

  fetchFilms();

